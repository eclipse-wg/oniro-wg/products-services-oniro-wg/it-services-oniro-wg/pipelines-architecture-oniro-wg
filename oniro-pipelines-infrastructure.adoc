:sectnumlevels: 4
:toclevels: 5
:sectnums: 4
:toc: left
:icons: font
:toc-title: Table of contents

= Oniro pipelines infrastructure

This document aim a better understanding of the Oniro pipelines infrastructure.

== Infrastructure Documentation  

=== Schema
image::img/infrastructure.png[Oniro Infrastructure]

```plantuml

@startuml
!pragma ratio 0.5
'skinparam padding 0
'skinparam nodesep 0
!theme spacelab

actor Developer

cloud EF as "EF cloud" {
  node gitef as "gitlab.eclipse.org"
  database artef as "Artifacts"
  gitef<->artef
}

cloud OSTC as "Oniro OSTC lab" {
  node dell as "Dell server" {
    component lxd1  as "LXD container" {
      component macaque as "gitlab-runner1" #yellowgreen
    }

    database stor1 as "local cache" #orange


    component lxd2 as "LXD container" {
      component champion as "gitlab-runner2" #yellowgreen
    }
    macaque-right->stor1
    champion-down->stor1
  }

  cloud hlaas as "Hardware Lab" {
    node rdut1 as "Raspberry Pi 4B"{
      component lavaw1 as "Lava worker"
    }
    node dut1 as "dut1"
    node dutN as "dutN"
  
    lavaw1-->dut1
    lavaw1-->dutN
    actor hadmin as "Hardware Admin"

    note bottom of hadmin
     Frequent hands-on activity
     on Devices Under Tests
    end note
  }

  note bottom of hlaas
   Multiple instances
  end note

  note bottom of stor1
    /cache
    /var/shared
    But in fact this is not shared.
    Each gitlab-runner has its own cache.
  end note
}

cloud OTC as "Open Telekom Cloud" {
  node ecs1 as "ECS server"{
    component gitotc as "git.ostc-eu.org" 
      database artotc as "gitlab artifacts"
      database regotc as "registry.git.ostc-eu.org"#orange
      gitotc-l->artotc
      gitotc->regotc
  }

  port otcapi as "OTC API"

  node docker1 as "ECS docker" {
    component comp1 as "docker compose"{
      component grafana as "grafana.ostc-eu.dev"
      component hawkbit as "hawkbit.ostc-eu.dev"
      component squad as "squadp.ostc-eu.dev"
    }
  }

  node ecsr1 as "ECS server"{
    component runner1 as "gitlab-runner 1" #yellowgreen
  }

  node rm as "ECS server" {
    component grm as "gitlab runner manager"#yellowgreen
    database cache as "/srv/cache" #yellowgreen
    grm->cache
  }

  note bottom of cache
   NFS export
   1TB /srv/cache
  end note

  note bottom of grm
    Orchestration of ECS servers deployment and deletion
  end note
  
  node ecsr2 as "ECS server"{
    component runnern as "gitlab-runner N" #yellowgreen
  }
  database s3 as "S3 storage"
  database s31 as "S3 storage"

  node lava as "lava.ostc-eu.org" {
    component lavas as "lava server"
  }
  runner1->cache
  runnern->cache
}

EF-[hidden]-->OTC

Developer-->gitef
gitef-->squad
squad-->lavas
gitef-->macaque
gitef-->champion
gitef-->hawkbit
hawkbit-d->s31
lavas->lavaw1
gitotc-d->s3

gitef-->regotc
gitef-->grm
grm-down->otcapi

gitef-->runner1
gitef-->runnern

@enduml

```

Or: https://git.ostc-eu.org/OSTC/infrastructure/infrastructure-maintenence/-/blob/main/README.md

=== Network flow matrix

@pawel : Network flow matrix

* schema?
* network flow matrix : kind (https,)
* what host, what type of communication, ...

ex : 

.Partial example of a network flows matrix
[cols="1e,2e,2e,2e,1e,1e"]
|====
| ID | Source | Destination | Network type | Protocol | Listening port

| 1 | lb2 | IP multicast 224.0.0.18 | LAN | VRRP over UDP | 3222
| 2 | lb1 | host1, host2 | LAN | HTTP | 80
| 3 | host3, host4, host5 | bdd1 | LAN | PG | 5432
| 4 | sup1 | host[1-6] | LAN | SNMP | 199
|====

List all thirds parties implies in the build process : 

* artifacts repositories, ex dockerhub, ...,
* sources : github, gitlab.com, ...
* CI pipeline : oniro gitlab repo, gitlab.com repo, ..
* test : lava infrastructure
* other : ...

image:https://gitlab.eclipse.org/eclipse/oniro-core/docs/-/raw/main/ci/assets/oniro-ci-flow.png[oniro ci flow]

====
NOTE: Schema don't show interraction with squadp
====

=== Network 

List here the possible network volume constraints used

[Example]
====
Example: The Ethernet network of the Datacenter has a bandwidth of 40 Gbps.
====

telecom cloud : quite fast
laboraty 200 MBPS, huawey center, swift router, several hardware labority, déploiement pi4, raspberry, C.

Hardware lab as a service blueprint : device undertest 


=== Versions of infrastructure components


[cols="1e,2e,1e,2e"]
|====
| Component | Role | Version | Technical environment

| runner
| build Oniro component
| 13.10!
| Ubuntu 20!
| lava server
| execute test 
| 9
| Debian 8, OpenJDK 1.8.0_144
|====

Ubuntu Lts by default

LXD container, of top of that. 

=== Listing flavor

@pawel: see if flavor listing are correct!

extract from pipelines : 
sizing, ex : s3.large.2!!!
https://support.huaweicloud.com/intl/en-us/productdesc-ecs/ecs_01_0014.html

vCPUs : 2
Memory (GiB) : 4
Bandwidth : 0.8/0.2
...

=== Sizing

Nothin to report.

==== Storage constraints


ex : Persistent storage

```
CI_ONIRO_RUNNER_PERSISTENT_STORAGE
CI_ONIRO_BB_LOCAL_CONF_DL_DIR: $CI_ONIRO_RUNNER_PERSISTENT_STORAGE/$CI_ONIRO_BUILD_CACHE/bitbake/downloads
CI_ONIRO_BB_LOCAL_CONF_SSTATE_DIR: $CI_ONIRO_RUNNER_PERSISTENT_STORAGE/$CI_ONIRO_BUILD_CACHE/bitbake/sstate-cache
```

Download directory and the sstate-cache

from https://git.ostc-eu.org/OSTC/infrastructure/ostc-gitlab-runner-config/-/blob/main/Makefile
```
$(localstatedir)/cache/$(Project.Name)/pub/bitbake.InstallMode = 2777
$(localstatedir)/cache/$(Project.Name)/pub/bitbake/sstate-cache.InstallMode = 2777
$(localstatedir)/cache/$(Project.Name)/pub/bitbake/downloads.InstallMode = 2777
$(localstatedir)/cache/$(Project.Name)/private/bitbake.InstallMode = 2777
$(localstatedir)/cache/$(Project.Name)/private/bitbake/sstate-cache.InstallMode = 2777
$(localstatedir)/cache/$(Project.Name)/private/bitbake/downloads.InstallMode = 2777
$(localstatedir)/cache/$(Project.Name)/tmp.InstallMode = 1777
```

arround 326 GB.

==== CPU constraints

32 cores of concurrency, divide on diffrent, 16 cores. by runner.

==== Memory constraints

No limit, depending on the system. 

=== Runner configuration

```VkyPqSaJ``` is currently paused.
[options="header,footer"]
|==================================================================================================================
| Runner  | Project                  | Runner Id  | Concurrency limit  | Memory    | Cores     | Jobs run  | Status
| 1       | Meta openHarmony         | 2qtqDPMF   | 4                  | No limit  | No limit  | 0         | Active
| 2       | Oniro Core Group runner  | sud5JQuy   | 8                  | No limit  | 32        | 1497      | Active
| 3       | Oniro                    | VkyPqSaJ   | 8                  | 16GB!!    | 16        | 164       | Paused
| 4       | openharmony-thierrye     | o6mFzc9T   | 2                  | No limit  | No limit  | 3         | Active
| 5       | OSTC runner              | S-MP9_bS   | 2                  | No limit  | 32        | 7         | Active
| 6       | openharmony-kristis      | n4unLFC9   | 8                  | No limit  | 32        | 25        | Active
| 7       | chase                    | UoSprHKM   | 2                  | 64GB      | 16        | 310       | Active
| 8       | openharmony-tony3oo3     | BEksC4Pc   | 2                  | No limit  | 32        | 0         | Active
| 9       | openharmony-chase        | ANk6P77V   | 2                  | No limit  | 32        | 24        | Active
| 10      | Docs                     | g-CB4CbJ   | 4                  | No limit  | 32        | 10        | Active
| 11      | landgraf                 | UcyM2XfK   | 2                  | No limit  | 32        | 0         | Active
| Sum     |                          |            | 44                 |           |           |           |       
|==================================================================================================================

Active only 
[options="header,footer"]
|==================================================================================================================
| Runner  | Project                  | Runner Id  | Concurrency limit  | Memory    | Cores     | Jobs run  | fork ?
| 2       | Oniro Core Group runner  | sud5JQuy   | 8                  | No limit  | 32        | 1497      | n     
| 7       | chase                    | UoSprHKM   | 2                  | 64GB      | 16        | 310       | y     
| 6       | openharmony-kristis      | n4unLFC9   | 8                  | No limit  | 32        | 25        | y     
| 9       | openharmony-chase        | ANk6P77V   | 2                  | No limit  | 32        | 24        | y     
| 10      | Docs                     | g-CB4CbJ   | 4                  | No limit  | 32        | 10        | n     
| 5       | OSTC runner              | S-MP9_bS   | 2                  | No limit  | 32        | 7         | n     
| 4       | openharmony-thierrye     | o6mFzc9T   | 2                  | No limit  | No limit  | 3         | y     
| Sum     |                          |            | 28                 |           |           |           |       
|==================================================================================================================



=== Static sizing

These are measured or estimated data that will be used as inputs to the calculation of required storage.

On gitlab => no idea currently, clean plan to be one week policy

Registry docker image : 67 GB

no tagging strategy on docker image.

=== Response time requirements

==== Duration of pipeline

External download, too long on EF gitlab .
Oniro look for a Squid proxy.

bitebake download take time, frequently timeout. 

Gitlab runner manager, 30 serveurs,
10 hours running. 


.Pipeline performance
[cols="1e,1e"]
|====
| project            | time  
| oniro              | ≈ 25min   
| sysota             | ≈ 12 - 16min
| eddie              | < 1 min  
| hlaas              | < 30s
| docs               | ≈ 2 - 8 min(last build!!)
| meta-openharmony   | ≈ 107min on 9 jobs, 53 min on 5 jobs
| meta-zephyr        | ≈ 8min
    
|====


==== schedule pipeline

Schedule job define in gitlab, run usually once a day, full build, full pipeline, with linaro, lava. 
check devise ok, devise available.
Turn on, Turning off, ...

see: duration of pipeline section

=== Gitlab runners configuration
The names of containers are randomly genated by LXD

==== knowing-macaque
The registrations for ```knowing-macaque``` have been moved to host system and is beeing re-organised.
```
listen_address = "0.0.0.0:9254"
concurrent = 16
check_interval = 10

[session_server]
  session_timeout = 1800

[[runners]]
  name = "dell-bm-for-openharmony"
  limit = 4
  output_limit = 100000
  url = "https://gitlab.eclipse.org/"
  token = "2qtqDP<..>"
  executor = "docker"
  environment = ["CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "knowing-macaque"
  limit = 4
  output_limit = 10000000
  url = "https://gitlab.eclipse.org/"
  token = "sud5JQ<..>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    cpus = "32"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm", "/dev/net/tun"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "knowing-macaque"
  limit = 4
  output_limit = 10000000
  url = "https://gitlab.eclipse.org/"
  token = "VkyPq<..>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    memory = "32GB"
    cpus = "16"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm", "/dev/net/tun"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "dell-bm-for-openharmony-thierrye"
  limit = 2
  output_limit = 100000
  url = "https://gitlab.eclipse.org/"
  token = "o6mFz<..>"
  executor = "docker"
  environment = ["CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "dell-bm-for-ostc"
  limit = 2
  output_limit = 100000
  url = "https://git.ostc-eu.org/"
  token = "S-MP9<..>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    cpus = "32"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "dell-bm-for-openharmony-kristis"
  limit = 8
  output_limit = 100000
  url = "https://gitlab.eclipse.org/"
  token = "n4unLF<..>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=4", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=4", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j8 -l8"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    cpus = "32"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "knowing-macaque"
  limit = 2
  output_limit = 100000
  url = "https://gitlab.eclipse.org/"
  token = "UoSpr<..>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    memory = "64GB"
    cpus = "16"
    privileged = false
    disable_entrypoint_overwrite = false
    cap_add = ["NET_ADMIN"]
    oom_kill_disable = false
    devices = ["/dev/kvm", "/dev/net/tun"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "dell-bm-for-openharmony-tony3oo3"
  limit = 2
  output_limit = 100000
  url = "https://gitlab.eclipse.org/"
  token = "BEksC<..>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=4", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=4", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j8 -l8"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    cpus = "32"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "dell-bm-for-openharmony-chase-mz"
  limit = 2
  output_limit = 100000
  url = "https://gitlab.eclipse.org/"
  token = "ANk6P<..>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    cpus = "32"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "dell-bm-for-docs"
  limit = 4
  output_limit = 100000
  url = "https://gitlab.eclipse.org/"
  token = "g-CB4<..>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    cpus = "32"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0

[[runners]]
  name = "dell-bm-for-landgraf"
  limit = 2
  output_limit = 100000
  url = "https://gitlab.eclipse.org/"
  token = "UcyM2X<..>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    cpus = "32"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/data/gitlab-runner/booting:/cache", "/data/oniro-ci:/var/shared:rw"]
    shm_size = 0
```
==== strong-mustang
LXD container
```
# lxc config show strong-mustang
architecture: x86_64
config:
  image.architecture: amd64
  image.description: Ubuntu focal amd64 (20220817_07:42)
  image.os: Ubuntu
  image.release: focal
  image.serial: "20220817_07:42"
  image.type: squashfs
  image.variant: cloud
  limits.cpu: 0-63
  volatile.base_image: 0d7e0fd2e25ba1f660394c82dc0a076af346f770421c2a44bc4b45729863045b
  volatile.cloud-init.instance-id: 596b236d-d6af-40ce-a1d5-304d3f067955
  volatile.eth0.host_name: veth682bc981
  volatile.eth0.hwaddr: 00:16:3e:34:e8:c2
  volatile.idmap.base: "0"
  volatile.idmap.current: '[{"Isuid":true,"Isgid":false,"Hostid":1000000,"Nsid":0,"Maprange":1000000000},{"Isuid":false,"Isgid":true,"Hostid":1000000,"Nsid":0,"Maprange":1000000000}]'
  volatile.idmap.next: '[{"Isuid":true,"Isgid":false,"Hostid":1000000,"Nsid":0,"Maprange":1000000000},{"Isuid":false,"Isgid":true,"Hostid":1000000,"Nsid":0,"Maprange":1000000000}]'
  volatile.last_state.idmap: '[{"Isuid":true,"Isgid":false,"Hostid":1000000,"Nsid":0,"Maprange":1000000000},{"Isuid":false,"Isgid":true,"Hostid":1000000,"Nsid":0,"Maprange":1000000000}]'
  volatile.last_state.power: RUNNING
  volatile.last_state.ready: "false"
  volatile.uuid: 568cf40b-277f-4a4c-b79d-2b9d8bbb4861
devices: {}
ephemeral: false
profiles:
- default-nvme1
- gitlab-runner
- proxy-9253
stateful: false
description: ""
```
config.toml:
```
listen_address = "0.0.0.0:9252"
concurrent = 16
check_interval = 10

[session_server]
  session_timeout = 1800

[[runners]]
  name = "strong-mustang"
  limit = 4
  output_limit = 100000
  url = "https://gitlab.eclipse.org/"
  token = "<...>"
  executor = "docker"
  environment = ["CI_ONIRO_NUM_CPUS=8", "CI_ONIRO_RUNNER_PERSISTENT_STORAGE=/var/shared", "GIT_SSL_NO_VERIFY=1", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=4", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_THREADS=8", "CI_ONIRO_BB_LOCAL_CONF_BB_TASK_IONICE_LEVEL=3.4", "CI_ONIRO_BB_LOCAL_CONF_PARALLEL_MAKE=-j32 -l128", "CI_ONIRO_BB_LOCAL_CONF_BB_NUMBER_PARSE_THREADS=32", "PASSTHROUGH_ADDITIONS=MAXLOAD_NINJA", "MAXLOAD_NINJA=128", "CI_ONIRO_PARALLEL_BUILD_MAX_JOBS=32", "CI_ONIRO_PARALLEL_BUILD_MAX_LOADAVG=128"]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    cpus = "32"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    devices = ["/dev/kvm", "/dev/net/tun"]
    disable_cache = false
    volumes = ["/dev/urandom:/dev/random", "/var/cache/booting:/cache", "/var/cache/oniro-ci:/var/shared:rw"]
    shm_size = 0
```
=== gitlab-runner registrations and resource allocations
More info https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/764#note_981794
