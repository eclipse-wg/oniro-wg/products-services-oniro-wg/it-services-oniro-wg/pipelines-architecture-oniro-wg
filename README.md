# pipelines architecture oniro working group


## Context

Oniro build processing is currently done at the OSTC infrastructure. As Oniro project must be vendor neutral, it has been decided to process all build at eclipse foundation's infrastructure.

Consequences: 

* Execute pipeline in gitlab EF instance (already done)
* Processing build in the EF infrastructure 
* Store artefacts in the artefact store service at EF. 

All existing pipeline from all oniro projects in gitlab EF instance, according to this specification, must be adapted.

## Goal of this project

This project aim to centralize all pipelines specification of the Oniro project, for the migration operation.

* Migration strategy
* Infrastructure definition 
* Gitlab runner configuration
* Pipelines configuration
* Externals services
* ...

