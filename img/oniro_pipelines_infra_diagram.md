# Oniro IP / License compliance toolchain diagram 

This is the automatically generated oniro ip/license compliance toolchain diagram. For a complete overview of configuration and requirements : https://git.ostc-eu.org/oss-compliance/pipelines

![IP compliance deployment diagram](http://www.plantuml.com/plantuml/png/dLJVQzim47wU_ugh-x0d1Pjs5CO8xKfWWR1bIzYU5VlgcCew8SaDPUH_tqc97NTkorO_dJv_t-cx7-ZQUMbzswZap5XPDHAiz3L1DlcGl7VtjJRIoWQCBCjQLv0DGKqbEZGCdlajDWZEo0ALt2Jy5OhQ4hoibIkaGlYTx3lmvjUAZn3rO4Zk74W7wPAShooklYzIvdRaR-lRIExuT-GSAQeUXR7rG_m5K51ZIAFsyS8yfbltL_jMQrJfaRl_Io7Pz1OFcfiOmvVrUWaExGFQbrGccirxuz7taBpl35UsY2midAXgp-qHDXYo8cHJjTG5fgCwAXj51m54mT3BgER_AjcHAcLKh49aB9LGIhVTaBJbgnMV1lLZjbe_fHgo7YwoB8ka4AGdFC-khY8UWe3pX46JHzYGzzG0tOLE1S5NWjbycG3gCZAIyUYvIFtiwBCtZFvDa_-7mQFQ26dgO3QoPulFwTYqSWVBYzMWMophL1nEE9xmLON0TaAs-dXvUH79HvuGKquJh-wNhlP1qEtGK1sKJ6TH614VAYjQ8wP7IP_Y7ViVhEYh9UJFrD0LyABvtdb-x5e86GaR4Xw0_ktun60SxXWQ8taY2qyMVjQuGmldZwWKxIgBg8z_vilRfrNDbp10FCecXVEDaiLzSiZ2gVcEq-7GzcIOYD7UIX9NH4pZqvPSeoxRHlq1)

```plantuml
@startuml
!pragma ratio 0.5
'skinparam padding 0
'skinparam nodesep 0
!theme spacelab



cloud tailscale {
}

cloud AWS {
  node poaws as "PostgreSQL"
}


cloud OTC {
  
  node fossology-priv {
    component tail1 as "Tailscale tunnel" {
    }
    component apache1 as "Apache HTTP server" {
    }
    component postgr1 as "PostreSQL" {
    }
  }
  node grc as "ecs-gitlab-runner-compliance" {
    component gr as "gitlab runner" {
    }
    component tail as "Tailscale tunnel" {
    }
    component solda as "Soldaprod dashboard" {
    }
    component postg as "PostgREST" {
    }
    port 3000 as "3000"
    port 1077 as "1077"
  }

 note bottom of grc
   No EIP
 end note

  node fossology-scan {
    component tail0 as "Tailscale tunnel" {
    }
    component apache as "Apache HTTP server" {
    }
    component postgr as "PostreSQL" {
    }
  }
  node elb-api-sca as "ELB" {
    component tls as "TLS"
    port apisca as "api.sca.ostc-eu.org:443"
    apisca-->tls
  }

  node gitostc as "git.ostc-eu.org"
  tls-->3000
  gr-up->gitostc

  node 	elb-compliance as "ELB" {
    component tls1 as "TLS"
    port sca as "sca.ostc-eu.org:443"
    sca-->tls1
  }
  tls1-->1077
}

tailscale<-->tail0
tailscale<-->tail
tailscale<-->tail1
actor Viewer #yellowgreen
actor IPCompliance as "IP Compliance team" #black
Viewer-->sca
Viewer-->apisca
IPCompliance-->tailscale
postg->poaws
@enduml
```
