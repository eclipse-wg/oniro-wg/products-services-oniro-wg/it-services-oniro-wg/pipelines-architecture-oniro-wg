coordination meeting 20220703

## Participants

* Pavel S. (Huawei)
* Sebastien H. (EF)
* Agustin B.B. (EF)
* Mikael B. (EF)

## Agenda

* Pipeline runner Infrastructure 
* Set up the schedule for next meeting about pipelines configuration

### Pipeline runner Infrastructure 

TODO : Network flow matrix: definition for technology protocol

Documentation for api communication, located inside projects : see in ci directory

Network performance : telecom cloud : quite fast
Laboraty 200 MBPS, huawey center, swift router, several hardware , raspberry PI deployment,...

TODO : Shared the link to dashboard grafana. 

OS : Ubuntu Lts by default,
LXD container, of top of that. 

TODO Project dependency : upstream project : extract the information. 
Note : no transfert between build project. 

Cache storage : 65 GB 
=> proxy cache to accelerate build processing

CPU :32 cores of concurrency, divide on diffrent, 16 cores. by runner.
RAM : No limit, depending on the system. 

Static sizing artifacts : On gitlab => no idea currently, clean plan to be one week policy

Versionning docker strategy : no tagging on docker image, only latest
Registry docker image : 67 GB

IMPORTANT : Bitebake download take time, frequently timeout. 

Gitlab runner manager, 30 serveurs,

Scheduled job define in gitlab, run usually once a day, full build, with linaro lava. 
device testing : check devise ok, devise available. Turn on, Turning off, ...


### Next schedule for this meeting series

Pawel busy planning:
    Thu 9 May – 2:30pm 

### Action

* Sebastien H.
  * Continue digging for infrastructure question (List all current build time, list schedule build, ...)
  * prepare next meeting
* Pawel S. 
  * Network flow matrix
  * Grafana access
  * Projects dependencies
  * Listing flavor 
