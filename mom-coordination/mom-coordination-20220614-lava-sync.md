coordination meeting 20220614

## Participants

* Stevan S. (Huawei)
* Sebastien H. (EF)

## Agenda


* Presentation of lava and squadp tooling

## Lava notes

Exemple file 
https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/blob/kirkstone/.oniro-ci/test-generic.yaml

handle : 
*  .lava-test: Submits a job to LAVA with CI variables
*  .lava-reports: get from squadp test result 
*  .lava-badge: generate badge base on squadp test result pourcentage 

https://squadp.svc.ostc-eu.dev
https://lava.ostc-eu.org/

CI variables : 
* CI_SQUAD_INSTANCE
* CI_SQUAD_TOKEN
* CI_LAVA_INSTANCE
* CI_LAVA_TOKEN


migration gitlab runner consist on  : creation of file send to lava and squadp
deploy image on device->boot device->listen from the ouput device, and get result back

see : 
curl $CI_SQUAD_INSTANCE/api/submitjob/oniro-core/$CI_PROJECT_NAME/$CI_PIPELINE_ID/$MACHINE --header "Auth-Token: $CI_SQUAD_TOKEN"  --form "backend=oniro_lava" --form "definition=@${job_def}"


When lava is finish, trigger gitlab pipeline with a callback
Incomplete job mean problem with infrastructure.

Both lava server(don't know where exactly) and squad (not sure), at ostc cloud.
=> confirm with Pawel

Documentation : 
https://docs.oniroproject.org/en/latest/oniro/ci/hidden-jobs/index.html
check if it updated about lava. 

squadp can get result from various platform. all store in database.

token gitlab store in squadp
check if it project access token 
ostcbot_token?
=> see with Pawel


### Action

* Sebastien H.
  * See with @Pawel S. about infrastructure