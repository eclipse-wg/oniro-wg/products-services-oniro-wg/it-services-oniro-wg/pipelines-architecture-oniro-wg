coordination meeting 20220703

## Participants

* Pavel S. (Huawei)
* Sebastien H. (EF)

## Agenda

* Pipeline configuration

### Pipeline runner Infrastructure 


squadp vs grafana ? EF migration?


hawkbit : OTA, upload distributions to hawkbit and update firmware target devices



Gitlab runner configuration : unprivileged mode, run with user UID 1000000
Get toml configuration from Pawel. 

### Next schedule 

not planned

### Action

* Sebastien H.
  * Continue on filling documentation
* Pawel S. 
  * Focus on tag @Pawel
